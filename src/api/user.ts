import request from "@/utils/request";

export interface LoginParamsData {
  username: string;
  password: string;
}

interface TokenData {
  token: string;
}

export interface UserInfoData {
  routes: string[];
  buttons: string[];
  roles: string[];
  name: string;
  avatar: string;
}

export default {
  // GET  /admin/acl/index/info
  // info
  info() {
    return request.get<any,UserInfoData>(`/admin/acl/index/info`);
  },

  // POST /admin/acl/index/login
  // login
  login(loginParams: LoginParamsData) {
    // 无论是get post delete put请求 想要约束返回数据的类型 需要传递泛型
    // 第一个泛型占位的 第二个泛型再约束真正返回数据的类型
    return request.post<any, TokenData>(`/admin/acl/index/login`, loginParams);
  },

  // POST /admin/acl/index/logout
  // logout
  logout() {
    return request.post<any, null>(`/admin/acl/index/logout`);
  },
};
