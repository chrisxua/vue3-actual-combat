import type { RouteRecordRaw } from "vue-router";
/**
 * 路由meta对象参数说明
 * meta: {
 *      title:          菜单栏及 tagsView 栏、菜单搜索名称（国际化）
 *      hidden：        是否隐藏此路由
 *      icon：          菜单、tagsView 图标，阿里：加 `iconfont xxx`，fontawesome：加 `fa xxx`
 * }
 */

/**
 * 静态路由（默认路由）
 * 所有的用户都可以看到的菜单路由
 */
export const staticRoutes: Array<RouteRecordRaw> = [
    {
        path: "/login",
        name: "Login",
        component: () => import("@/views/login/index.vue"),
        meta: {
            hidden: true,
        },
    },

    {
        path: "/404",
        name: "404",
        component: () => import("@/views/error/404.vue"),
        meta: {
            hidden: true,
        },
    },

    // 对于咱们的一级路由而言生成一级菜单 二级路由来去生成二级菜单
    // 如果一级路由重只有一个二级路由 那么二级路由生成的菜单会直接替代调一级路由生成的菜单
    {
        path: "/",
        component: () => import("@/layout/index.vue"),
        redirect: "/home",
        children: [
            {
                path: "home",
                name: "Home",
                component: () => import("@/views/home/index.vue"),
                meta: {
                    title: "首页", // 标题
                    icon: "ele-HomeFilled", //图标
                },
            },
        ],
    },

    // 咱们要去搭建的菜单 需要一个一级路由以及她下面的四个二级路由(品牌 属性 spu sku)
    // 一级路由需要有图标和标题 二级路由菜单需要显示标题
    // 咱们自己配的一级路由菜单功能 点击展开或者收缩下面的二级菜单 不需要重定向
    // 二级路由组件的出口是在AppMain这个组件中
    // 咱们定义出来的所有路由都需要增加一个名字(name) 咱们之后是会讲权限管理的 他当中需要用到这个名字 不能乱写 首字母还需要大写


    /* 匹配任意的路由 必须最后注册 */
];

/**
 * 定义动态路由
 * 根据用户登录返回的路由权限信息决定动态注册的路由
 */
export const allAsyncRoutes: Array<RouteRecordRaw> = [
    {
        path: "/acl",
        name: "Acl",
        component: () => import("@/layout/index.vue"),
        redirect: "/acl/user/list",
        meta: {
            title: "权限管理",
            icon: "ele-Setting",
        },
        children: [
            {
                name: "User",
                path: "/acl/user/list",
                component: () => import("@/views/acl/user/index.vue"),
                meta: {
                    title: "用户管理",
                },
            },
            {
                name: "Role",
                path: "/acl/role/list",
                component: () => import("@/views/acl/role/index.vue"),
                meta: {
                    title: "角色管理",
                },
            },
            {
                name: "RoleAuth",
                path: "/acl/role/auth",
                component: () => import("@/views/acl/role/roleAuth.vue"),
                meta: {
                    title: "角色管理",
                    hidden: true,
                    activeMenu: "/acl/role/list",
                },
            },
            {
                name: "Permission",
                path: "/acl/permission/list",
                component: () => import("@/views/acl/permission/index.vue"),
                meta: {
                    title: "菜单管理",
                },
            },
        ],
    },
    {
        path:'/product',
        component: () => import("@/layout/index.vue"),
        name:'Product',
        meta: {
            title: "商品管理", // 标题
            icon: "ele-Goods", //图标
        },
        children:[
            {
                path:'trademark/list',
                component: () => import("@/views/product/trademark/index.vue"),
                meta: {
                    title: "品牌管理", // 标题
                },
                name:'Trademark'
            },
            {
                path:'attr/list',
                component: () => import("@/views/product/attr/index.vue"),
                meta: {
                    title: "平台属性管理", // 标题
                },
                name:'Attr'
            },
            {
                path:'spu/list',
                component: () => import("@/views/product/spu/index.vue"),
                meta: {
                    title: "spu管理", // 标题
                },
                name:'Spu'
            },
            {
                path:'sku/list',
                component: () => import("@/views/product/sku/index.vue"),
                meta: {
                    title: "sku管理", // 标题
                },
                name:'Sku'
            },
        ]
    },
];

// 任意路由
// 这个路由是用户随意输入路径的时候要匹配的，最终重定向到404,注册的时候，必须注册在最后一个

export const anyRoute = {
    path: "/:pathMatch(.*)",
    name: "Any",
    redirect: "/404",
    meta: {
        hidden: true,
    },
}
